---
title: Moment Based Rendering
subtitle: Master final project
abstract: The purpose of this project was to implement two recent rendering approaches for simulating cast shadows and transparency effects in real-time, and evaluate them in comparison with state-of-the-art older techniques. These techniques leverage the statistical moments of the fragment depth to approximate the visibility and transmittance functions.
img_pres: /assets/images/moment_based_rendering/sponza-2.png
main_page: http://fsi-dpt-info.univ-tlse3.fr/master-igai/2018-g1
gitlab_url: https://gitlab.com/PierreMezieres/Rogue/tree/CO-master
report: /extern/moment_based_rendering/MBR-Recette.pdf
---
