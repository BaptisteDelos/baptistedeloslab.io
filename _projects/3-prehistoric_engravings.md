---
title: Digital Imaging and Prehistoric Carved Stones
subtitle: First year Master internship · <a href="http://manao.inria.fr/" target="_blank">Manao team</a> · Inria Bordeaux Sud-Ouest
abstract: The aim of this project was to elaborate a 3D acquisition pipeline suited for engraving detection on Paleolithic carved stones.
img_pres: /assets/images/prehistoric_engravings/pebble_3d.png
gitlab_url: https://gitlab.com/BaptisteDelos/mesh_proc
---
