---
title: Lighting Design for Appearance Manipulation
subtitle: Last year Master internship · <a href="http://manao.inria.fr/" target="_blank">Manao team</a> · Inria Bordeaux Sud-Ouest
abstract: This project proposed to exploit research studies in the field of visual perception, focused on the influence of lighting on the perception of shapes and materials, in order to guide the process of lighting design.
img_pres: /assets/images/perceptual_lighting_design/lighting_design.png
gitlab_url: https://gitlab.inria.fr/pacanows/MRF/tree/baptiste
report: /extern/perceptual_lighting_design/rapport-de-stage.pdf
---
