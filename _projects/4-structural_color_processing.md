---
title: Structural Color Point Clouds Processing
subtitle: Third yeard licence internship · <a href="https://www.irit.fr/STORM/site" target="_blank">STORM team</a> · IRIT
abstract: This project was carried out during my internship in the IRIT STORM research team in 2017. These works are based on the observation that the distribution of an image's colors in the RGB space depicts distinct structures. The envisaged approach proposes to exploit this representation for image color manipulation.
img_pres: /assets/images/structural_color_processing/point_cloud.png
article: https://arxiv.org/abs/1912.04583
main_page: /extern/structural_color_processing/work_presentation.html
demo: https://baptistedelos.gitlab.io/structural-color-processing
---
